package com.example.student1.twoactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bClicker;
    EditText logtext,pastext,returnpas;
    //TextView t;
    TextView pText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //t.setText(R.string.hint_password);

        bClicker = (Button) findViewById(R.id.button_clicker);
        logtext = (EditText) findViewById(R.id.login_text);
        pastext = (EditText) findViewById(R.id.pass_text);
        returnpas = (EditText) findViewById(R.id.returnpass_text);
        pText = (TextView) findViewById(R.id.proverka_text);

        bClicker.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        pText.setText("Button work");

        String username = logtext.getText().toString();

        Intent intent = new Intent(this,GrettingActivity.class);

        intent.putExtra("login", username);

        startActivity(intent);
    }




}
