package com.example.student1.twoactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class GrettingActivity extends AppCompatActivity {

    TextView tGreetings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gretting);

        tGreetings = (TextView) findViewById(R.id.t_greetings);

        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");

        tGreetings.setText("Hello , " + username);

    }

}
